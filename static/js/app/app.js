function App() {};

App.prototype.createTerminal = function() {
    this.fitTerminal();
    var font_size = parseFloat($('#terminal').css('font-size'));
    var rows = Math.round($('#terminal').height()/(font_size*1.5));

    this.term = new Terminal({
        cols: 80,
        rows: rows,
        useStyle: false,
        screenKeys: false
    });

    this.term.open(document.getElementById("terminal"));
    $('.terminal').css('margin-left', font_size);
    $('.terminal').css('margin-top', font_size);
};

App.prototype.fitTerminal = function() {
    var terminal = $('#terminal');
    var terminal_class = $('.terminal');
    var instructions = $('#instructions');

    if (instructions.height() > $(window).height()) {
        terminal.height(instructions.height());
    } else {
        terminal.height(
            $(window).height() - $('.navbar').height()
        );
    }

    if (terminal_class.height() > terminal.height()) {
        terminal.height(terminal_class.height());
    }

    var font_size = Math.round(terminal.width()/54);
    if (font_size < 9) {
        font_size = 9;
    } else if (font_size > 16) {
        font_size = 16;
    }

    terminal.css('font-size', font_size);
    $('.terminal').css('margin-left', font_size);
};

App.prototype.connectTerminal = function(url) {
    var self = this;
    self.sock = new WebSocket(url);

    this.sock.onopen = function() {
        self.term.on('data', function(data) {
            self.sock.send(data);
            if (data.charCodeAt(0) == 13) {
                self.checkVictory();
            }
        });

        self.sock.onmessage = function(event) {
          var data = event.data;
          self.term.write(data);
          if (data.indexOf("[0m") !== -1) {
                self.checkVictory();
          }
        };

        self.sock.onclose = function() {
            self.term.write('\r\n\nServer connection closed.')
        };

        self.term.write("\r\n");

    };
};

App.prototype.disconnectTerminal = function() {
    this.term.off('data');
    this.sock.close();
};

App.prototype.getTerminalRows = function() {
    var rows = [];
    var last_non_empty_input = -1;

    $(".terminal > div").each(function(index, elem) {
        var row = $('<div/>').html(elem.innerHTML).text();
        row = row.replace(/^\s+|\s+$/g, '');

        rows.push(row);
        if (row.search(/In\s\[\d+\]:\s.+/) != -1) {
            last_non_empty_input = index;
        };
    });

    if (last_non_empty_input != 1) {
        return rows.slice(last_non_empty_input - 1)
    }
    return rows
};

App.prototype.victoryConditions = {
    conditions: [],
    final: undefined
}

App.prototype.won = false;

App.prototype.checkVictory = function () {
    var rows = this.getTerminalRows();
    var conditions = [];

    this.victoryConditions.conditions.forEach(function(elem) {
        if (typeof(elem.condition) == "function") {
            var status = elem.condition(rows);
            if (status) {
                if (elem.postCondition) {
                    elem.postCondition();
                }
            } else {
                conditions.push(elem);
            }
        } else if (elem.ordered){
            var status = elem.condition[0](rows);
            if (status) {
                elem.condition = elem.condition.slice(1);
            }

            if (elem.condition.length == 0 && elem.postCondition) {
                elem.postCondition();
            } else {
                conditions.push(elem);
            }
        } else {
            var sub_conditions = [];
            elem.condition.forEach(function(elem) {
                var status = elem(rows);
                if (!status) {
                    sub_conditions.push(elem);
                }
            });

            if (sub_conditions.length == 0) {
                elem.postCondition();
            } else {
                elem.condition = sub_conditions;
                conditions.push(elem);
            }
        }
    });

    this.victoryConditions.conditions = conditions;
    if (conditions.length == 0 && !this.won) {
        this.victoryConditions.final();
        this.won = true;
    }
};

App.prototype.getCommandIndex = function(expression, rows) {
    var regex = new RegExp(expression);
    var indexes = [];

    rows.forEach(function(elem) {
        if (elem.search(regex) != -1) {
            var match = elem.match(/\d+/);
            if (match != null) {
                indexes.push(match[0]);
            } else {
                indexes.push('output');
            }
        }
    });

    if (indexes.length != 0) {
        return indexes
    }
}

App.prototype.rowContains = function(expression) {
    var self = this;
    var condition = function(rows) {
        var indexes = self.getCommandIndex(expression, rows);
        if (indexes) {
            return true
        }
        return false
    };

    return condition
}

App.prototype.matchInput = function(expression) {
    var expression = '^In\\s\\[\\d+\\]:\\s' + expression;
    return this.rowContains(expression);
}

App.prototype.matchOutput = function(expression) {
    var expression = '^Out\\[\\d+\\]:\\s' + expression;
    return this.rowContains(expression);
}

App.prototype.matchIO = function(iexp, oexp) {
    var self = this;
    var status = false;
    var condition = function(rows) {
        var icomm = self.getCommandIndex('^In\\s\\[\\d+\\]:\\s' + iexp, rows);
        if (icomm) {
            var ocomm = self.getCommandIndex('^Out\\[\\d+\\]:\\s' + oexp, rows);
            if (ocomm) {
                icomm.forEach(function(elem) {
                    if (ocomm.indexOf(elem) != -1) {
                        status = true;
                    }
                });
            }
        }

        return status
    };

    return condition
}