from subprocess import Popen, PIPE
from collections import defaultdict
import weakref
import os
import fcntl
import signal
import time
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.gen
import tornado.template
from tornado.options import define, options, parse_command_line
import docker
from utils import spawn_thread
from levels import LEVELS
import ascii


TEMPLATE_DIR = os.path.join(os.path.dirname(__file__), 'templates')


class HtmlHandler(tornado.web.RequestHandler):
    loader = tornado.template.Loader(TEMPLATE_DIR)
    templates = os.listdir(TEMPLATE_DIR)
    templates.remove('base.html')

    @tornado.web.asynchronous
    def get(self, level):
        level = level.rstrip('/')
        if not level:
            level = '1'

        f = level + '.html'
        if f not in self.templates:
            self.send_error(status_code=404)
        else:
            body = self.loader.load(level + '.html').generate(level=level)
            self.finish(body)


class WebSocketHandler(tornado.websocket.WebSocketHandler):
    __refs__ = defaultdict(list)
    client = docker.Client(
        base_url='unix://var/run/docker.sock',
        version='1.9',
        timeout=10
    )

    @classmethod
    def get_instances(cls):
        for inst_ref in cls.__refs__[cls]:
            inst = inst_ref()
            if inst is not None:
                yield inst

    def _handle_server_error(self):
        self.write_message(ascii.error)
        self.teardown(close=True)

    def _create_container(self, image_id):
        self.container = self.client.create_container(
            image_id,
            stdin_open=True,
            tty=True,
            command='ipython',
            user='ipython',
            hostname='ipython',
            mem_limit=48*524288,
            network_disabled=True,
            cpu_shares=1,
            name='ipython%s' % time.time(),
            environment={'IPYTHONDIR': '/home/ipython'},
        )

        self.client.start(self.container)

    def _attach_to_container(self):
        self.app = Popen('docker attach ' + self.container['Id'],
                         shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE,
                         preexec_fn=os.setsid)
        fcntl.fcntl(self.app.stdout.fileno(), fcntl.F_SETFL, os.O_NONBLOCK)

        self.ioloop = tornado.ioloop.IOLoop.instance()
        self.ioloop.add_handler(
            self.app.stdout.fileno(),
            self.read_and_send,
            self.ioloop.READ
        )

    @spawn_thread
    def teardown(self, close=False):
        self.ready = False
        self.ioloop.remove_handler(self.app.stdout.fileno())
        os.killpg(self.app.pid, signal.SIGTERM)

        try:
            self.client.kill(self.container['Id'], signal=9)
            self.client.remove_container(self.container['Id'])
        except:
            pass

        if close:
            self.close()

    def read_and_send(self, fd, event):
        if self.app.poll() is not None or not self.ready:
            self._handle_server_error()
            return

        try:
            proc_output = self.app.stdout.read()
        except IOError:
            pass
        else:
            self.write_message(proc_output)

    @spawn_thread
    def open(self, level_id):
        self.__refs__[self.__class__].append(weakref.ref(self))
        self.ready = False

        level = LEVELS.get(level_id)
        if not level:
            self.close()
            return

        self._create_container(level.image)
        self._attach_to_container()
        self.ready = True

    def on_message(self, message):
        if self.ready:
            try:
                self.app.stdin.write(message)
            except IOError:
                self._handle_server_error()

    def on_close(self):
        self.teardown()


def cleanup():
    for socket_handler in WebSocketHandler.get_instances():
        socket_handler.teardown(close=True)


if __name__ == '__main__':
    define("port", default=8888, help="run on the given port", type=int)
    parse_command_line()

    tornado.web.Application([
        (r'/ws/(\d{1,64})', WebSocketHandler),
        (r'/static/(.*)', tornado.web.StaticFileHandler, {'path': 'static/'}),
        (r'/(.*)', HtmlHandler),
    ]).listen(options.port)

    try:
        tornado.ioloop.IOLoop.instance().start()
    finally:
        cleanup()