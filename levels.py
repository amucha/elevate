from collections import namedtuple


Level = namedtuple('Level', ['image'])

LEVELS = {
    '1': Level('eb9e9693f839'),
    '2': Level('eb9e9693f839'),
    '3': Level('eb9e9693f839'),
    '4': Level('eb9e9693f839'),
    '5': Level('eb9e9693f839'),
    '6': Level('eb9e9693f839'),
    '7': Level('eb9e9693f839'),
    '8': Level('eb9e9693f839'),
    '9': Level('eb9e9693f839'),
    '10': Level('eb9e9693f839'),
}